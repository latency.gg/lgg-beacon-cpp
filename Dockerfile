FROM alpine:3.14 AS builder

RUN apk add --no-cache \
    "build-base" \
    "musl-dev" \
    "cmake" \
    "curl-static>7.79.1" \
    "curl-dev>7.79.1" \
    "brotli-static>=1.0.0" \
    "brotli-dev>=1.0.0" \
    "nghttp2-dev>1.43.0" \
    "nghttp2-static>1.43.0" \
    "boost-dev>1.76.0" \
    "boost-build>1.76.0" \
    "boost-static>1.76.0" \
    "openssl-dev>1.1.0" \
    "openssl-libs-static>1.1.0" \
    "zlib-dev>1.2.0" \
    "zlib-static>1.2.0" \
    "libsodium-dev>1.0.18" \
    "libsodium-static>1.0.18" \
    "nlohmann-json>3.9.0" \
    clang-extra-tools \
    dpkg

WORKDIR /root

COPY src /root/src
COPY packages /root/packages
COPY CMakeLists.txt Makefile /root/

RUN make


FROM alpine:3.14

COPY --from=builder /root/.build/release/beacon /root/beacon

ENTRYPOINT [ "/root/beacon" ]
