//
// Created by tim on 24/09/2021.
//
#include <vector>
#include <memory>
#include <boost/asio.hpp>
#include <sodium.h>
#include "dataping.h"


#ifndef UDP_SERVER_H
#define UDP_SERVER_H
using boost::asio::ip::udp;

namespace LatencyGG::Beacon {

    class UDPServer {
        std::shared_ptr<std::mutex> mSocket1Claim;
        std::shared_ptr<std::mutex> mSocket2Claim;
        std::shared_ptr<udp::socket> mSocket1;
        std::shared_ptr<udp::socket> mSocket2;
        std::vector<std::shared_ptr<DataPing>> mPings;
        std::vector<std::thread> mThreads;
        std::shared_ptr<std::mutex> mIncompleteClaim;
        std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<DataPingState>>> mIncomplete;
        unsigned char mSigPk[crypto_sign_PUBLICKEYBYTES];
        unsigned char mSigSk[crypto_sign_SECRETKEYBYTES];
        unsigned char mEncPk[crypto_scalarmult_curve25519_BYTES];
        unsigned char mEncSk[crypto_scalarmult_curve25519_BYTES];

        std::thread mGCThread;
        std::mutex mGCLock;
        bool mGCAlive = false;
        void garbageCollector();
    public:
        std::shared_ptr<std::mutex> mCompleteClaim;
        std::shared_ptr<std::unordered_multimap<timestamp_millisec_t, std::shared_ptr<DataPingState>>> mComplete;

        std::string getPublicKey();

        UDPServer(std::shared_ptr<udp::socket> &aSocket1, std::shared_ptr<udp::socket> &aSocket2, size_t aThreads);

        void serveForever();

        void kill();
    };

}

#endif // UDP_SERVER_H
