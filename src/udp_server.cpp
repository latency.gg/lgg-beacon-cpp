//
// Created by tim on 24/09/2021.
//

#include <csignal>
#include <chrono>
#include <thread>
#include <boost/asio.hpp>
#include <iostream>

#include "udp_server.h"

using namespace std::chrono_literals;
using namespace LatencyGG::Beacon;

UDPServer::UDPServer(std::shared_ptr<udp::socket> &aSocket1, std::shared_ptr<udp::socket> &aSocket2, size_t aThreads) {
    if (sodium_init() < 0) {
        throw std::runtime_error("Could not safely initialise libsodium");
    }
    crypto_sign_keypair(mSigPk, mSigSk);
    crypto_sign_ed25519_pk_to_curve25519(mEncPk, mSigPk);
    crypto_sign_ed25519_sk_to_curve25519(mEncSk, mSigSk);
    mSocket1 = aSocket1;
    mSocket2 = aSocket2;
    mSocket1Claim = std::make_shared<std::mutex>();
    mSocket2Claim = std::make_shared<std::mutex>();
    mComplete = std::make_shared<std::unordered_multimap<timestamp_millisec_t, std::shared_ptr<DataPingState>>>();
    mCompleteClaim = std::make_shared<std::mutex>();
    mIncomplete = std::make_shared<std::unordered_map<std::string, std::shared_ptr<DataPingState>>>();
    mIncompleteClaim = std::make_shared<std::mutex>();
    for (size_t i = 0; i < aThreads; i++) {
        mPings.push_back(
                std::make_shared<DataPing>(mSocket1, mSocket1Claim, mComplete, mCompleteClaim, mIncomplete,
                                           mIncompleteClaim, mSigPk, mSigSk, mEncPk, mEncSk));
        mThreads.emplace_back(&DataPing::run, &(*mPings[i]));
    }
    for (size_t i = aThreads; i < aThreads * 2; i++) {
        mPings.push_back(
                std::make_shared<DataPing>(mSocket2, mSocket2Claim, mComplete, mCompleteClaim, mIncomplete,
                                           mIncompleteClaim, mSigPk, mSigSk, mEncPk, mEncSk));
        mThreads.emplace_back(&DataPing::run, &(*mPings[i]));
    }
}

void UDPServer::serveForever() {
    {
        std::lock_guard<std::mutex> lock(mGCLock);
        mGCAlive = true;
    }
    mGCThread = std::thread(&UDPServer::garbageCollector, this);
    std::for_each(mThreads.begin(), mThreads.end(), std::mem_fn(&std::thread::join));
    mGCThread.join();
}

void UDPServer::kill() {
    std::lock_guard<std::mutex> lock(mGCLock);
    mGCAlive = false;
    std::for_each(mPings.begin(), mPings.end(), std::mem_fn(&DataPing::kill));
}

void UDPServer::garbageCollector() {
    while (true) {
        auto current_ts = DataPing::now();
        {
            std::lock_guard<std::mutex> lock_temp_storage(*mIncompleteClaim);
            std::vector<std::string> records;
            for (auto &&[key, dps]: *mIncomplete) {
                std::chrono::milliseconds initial_ts(dps->mTimestampsBeacon[0]);
                if (current_ts - initial_ts >= (5s)) {
                    std::cout << dps->mTimestampsBeacon[0] << std::endl;
                    records.push_back(key);
                }
            }
            for (auto &key: records) {
                std::cout << "request " << key << " timed out" << std::endl;
                mIncomplete->erase(key);
            }
        }

        {
            std::lock_guard<std::mutex> lock_killswitch(mGCLock);
            if (!mGCAlive) {
                break;
            }
        }
        std::this_thread::sleep_for(1s);
    }
}

std::string UDPServer::getPublicKey() {
    char publickey_b64[45];
    sodium_bin2base64(publickey_b64, 45, this->mEncPk, crypto_sign_PUBLICKEYBYTES, sodium_base64_VARIANT_ORIGINAL);
    return std::string(publickey_b64);
}



