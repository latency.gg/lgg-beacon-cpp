//
// Created by tim on 15/08/2021.
//
#include <vector>
#include <cstdint>
#include <memory>

#ifndef DATAPING_WIRE_HPP
#define DATAPING_WIRE_HPP

using timestamp_millisec_t = uint64_t;

#define LATENCYGG_TOKEN_LEN 161
#define LATENCYGG_TOKEN_DECODED_LEN 112
#define LATENCYGG_TOKEN_DECRYPTED_LEN 64
#define LATENCYGG_IDENT_LEN 89
#define LATENCYGG_SIG_LEN 89

enum data_ping_response_type_t : int32_t {
    eSNull = 0, eSInit, eSNext, eSFinal = 255
};

enum data_ping_request_type_t : int32_t {
    eCInit = 1, eCSecond, eCFinal = 255
};

enum : int32_t {
    LATENCYGG_DATA_PING_REQUEST_LEN = 192
};

enum : int32_t {
    LATENCYGG_DATA_PING_RESPONSE_LEN = 112
};

using signature_t = char[LATENCYGG_SIG_LEN];
using token_t = char[LATENCYGG_TOKEN_LEN];
using ident_t = char[LATENCYGG_IDENT_LEN];

struct DataPingRequestV2 {
    /*
     * |00 01 02 03|04 05 06 07|08 09 10 11 12 13 14 15|
     * +-----+-----+-----------+-----+-----------------+
     * |  mVersion |   mType   | mSeq|                 |
     * +-----+-----+-----------+-----+-               -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                     mIdent                    |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-   -+-----------------+-----------------------+
     * |     |     UNUSED      |      mtimestamp       |
     * +-----+-----------------+-----------------------+
     */
    int32_t mVersion;
    data_ping_request_type_t mType;
    uint16_t mSeq;
    ident_t mIdent;
    timestamp_millisec_t mTimestamp;

    std::shared_ptr<DataPingRequestV2> static deserialize(std::vector<uint8_t> &aData);
};

struct DataPingRequestV3 {
    /*
     * |00 01 02 03|04 05 06 07|08 09 10 11 12 13 14 15|
     * +-----------+-----------+-----+-----------------+
     * |  mVersion |   mType   | mSeq|      UNUSED     |
     * +-----------+-----------+-----+-----------------+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                     mToken                    |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-----------------------+-----------------------+
     * |         UNUSED        |      mtimestamp       |
     * +-----------------------+-----------------------+
     */
    int32_t mVersion;
    data_ping_request_type_t mType;
    uint16_t mSeq;
    token_t mToken;
    timestamp_millisec_t mTimestamp;

    std::shared_ptr<DataPingRequestV3> static deserialize(std::vector<uint8_t> &aData);
};

class DataPingRequestContainer {
private:
    int32_t mVersion;
    std::shared_ptr<DataPingRequestV2> dpv2;
    std::shared_ptr<DataPingRequestV3> dpv3;
public:
    int32_t version();
    data_ping_request_type_t type();
    uint16_t seq();
    char * identOrToken();
    timestamp_millisec_t timestamp();

    std::shared_ptr<DataPingRequestContainer> static deserialize(std::vector<uint8_t> &aData);
};

struct DataPingTokenV1{
    std::string mIp;
    timestamp_millisec_t mTimestamp;


    std::shared_ptr<DataPingTokenV1> static decode(unsigned char *aData);
};


struct DataPingResponseV2{
    /*
     * |00 01 02 03|04 05 06 07|08 09 10 11 12 13 14 15|
     * +-----------+-----------+-----+-----------------+
     * |  mVersion |   mType   | mSeq|                 |
     * +-----------+-----------+-----+-               -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                  mSignature                   |
     * +-                                             -+
     * |                                               |
     * +-                                             -+
     * |                                               |
     * +-   -+-----------------+-----------------------+
     * |     |     UNUSED      |      mtimestamp       |
     * +-----+-----------------+-----------------------+
     */
    int32_t mVersion;
    data_ping_response_type_t mType;
    uint16_t mSeq;
    signature_t mSignature;
    timestamp_millisec_t mTimestamp;

    std::vector<uint8_t> serialize();
};


#endif // DATAPING__WIRE_HPP
